<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 7/11/2019
  Time: 09:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script></script>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Edit User</h4>
</div>
<div class="modal-body">
    <div class="form-group">
        <label>Name</label>
        <input class="form-control" id="nameIdU" placeholder="Name" type="text" value="${user.name}">
    </div>
    <div class="form-group">
        <label>Surname</label>
        <input class="form-control" id="surnameIdU" placeholder="Surname" type="text" value="${user.surname}">
    </div>
    <%--<div class="form-group">
        <label>Date of Birth</label>
        <input class="form-control" id="dobId" placeholder="Date" type="text">
    </div>--%>
    <div class="form-group">
        <label>Age</label>
        <input class="form-control" id="ageIdU" placeholder="Age" type="text" value="${user.age}">
    </div>
    <div class="form-group">
        <button type="button" id="updateBtnId" class="btn btn-block btn-primary btn-flat">Edit</button>
    </div>

</div>
<%--
<div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" >Save changes</button>
</div>--%>
