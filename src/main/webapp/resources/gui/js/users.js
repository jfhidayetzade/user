var globBtnUserId='';
$(function () {
    $('#usersId').DataTable({
        'paging'      : false,
        'lengthChange': false,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : true
    });


});

$(function () {

    $('#saveBtnId').click(function () {

        addUser();
    });

    $(document).on('click','#updateBtnId', function () {

        updateUser();
    })
})

function addUser() {
        var name=$('#nameId').val();
        var surname=$('#surnameId').val();
        /*var dob=$('#dobId').val();*/
        var age=$('#ageId').val();

        var data = {
            name:name,
            surname:surname,
            /*dob:dob,*/
            age:age
        };
        $.ajax({
            url:'addUser',
            type:'POST',
            data:data,
            success:function (data) {
               if(data=='true'){
                   alert ('success');
               }else{
                   alert ('0 and negative numbers can not be entered.!');
               }

            }

        })

}

function editUser(userId) {
    globBtnUserId=userId;
    $.ajax({
        url:'editUser',
        type:'GET',
        data: 'userId='+userId,
        dataType: 'html',
        success: function (data) {
            $('.modal-content').html(data);
            $('#userModalId').modal('show')
        }
    })
}

function updateUser() {
    var name=$('#nameIdU').val();
    var surname=$('#surnameIdU').val();
    /*var dob=$('#dobId').val();*/
    var age=$('#ageIdU').val();

    var data = {
        name:name,
        surname:surname,
        /*dob:dob,*/
        age:age,
        userId:globBtnUserId
    };
    $.ajax({
        url:'updateUser',
        type:'POST',
        data:data,
        success:function (data) {
            if(data=='true'){
                alert ('success');
            }else{
                alert ('age 0 and negative numbers can not be changed!');
            }

        }

    })

}

function deleteUser(userId) {
    var  isDelete=confirm("Are you sure?")

    if(isDelete){
        $.ajax({
            url:'deleteUser',
            type:'POST',
            data:'userId='+userId,
            success:function (data) {
                if(data=='true'){
                    alert ('success');
                }else{
                    alert ('error');
                }

            }

        })
    }
}

