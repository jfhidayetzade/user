package az.spring.mvc.controller;

import az.spring.mvc.model.User;
import az.spring.mvc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import java.util.*;
import java.text.*;

@Controller
public class MainController {

        @Autowired
        private UserService userService;

        DateFormat df=new SimpleDateFormat("MM/dd/yyyy");

//        @Autowired
//        private DoctorService doctorService;

        @RequestMapping(value = {"/","index"}, method = RequestMethod.GET)
        public ModelAndView index(){
                ModelAndView modelAndView=new ModelAndView("main");

                try {
                        modelAndView.addObject("userList", userService.getUserList());
                } catch (Exception e) {
                        e.printStackTrace();
                }
//                System.out.println("Salam");

                return  modelAndView;
        }
        @RequestMapping(value ="/spring/usersList", method = RequestMethod.GET)
        public ModelAndView userList(){
                ModelAndView modelAndView=new ModelAndView("user/userList");

                try {
                        List<User> userList = userService.getUserList();
                        modelAndView.addObject("userList", userService.getUserList());
                } catch (Exception e) {
                        e.printStackTrace();
                }


                return  modelAndView;
        }
        @RequestMapping(value ="/spring/newUser", method = RequestMethod.GET)
        public ModelAndView newUser(){
                ModelAndView modelAndView=new ModelAndView("user/newUser");

                try {
                        modelAndView.addObject("userList", userService.getUserList());
                } catch (Exception e) {
                        e.printStackTrace();
                }


                return  modelAndView;
        }

        @RequestMapping(value = "/spring/addUser", method = RequestMethod.POST)
        public @ResponseBody String addUser(@RequestParam("name") String name,@RequestParam("surname") String surname,
                  @RequestParam("age") int age){
                String result="false";
                User user =new User();

                try {
                        user.setName(name);
                        user.setSurname(surname);
                       /* user.setDob(df.parse(dob));*/

                        user.setAge(age);
                        boolean isUser= userService.addUser(user);
                        if(isUser){
                            result="true";
                        }
                } catch (Exception e) {
                        e.printStackTrace();
                }
                return result;
        }

        @RequestMapping(value = "/spring/editUser",method = RequestMethod.GET)
        public ModelAndView editUser(@RequestParam("userId") Long userId){

             ModelAndView model=new ModelAndView();
             model.setViewName("user/editUser");
             User user = userService.getUserById(userId);
                model.addObject("user", user);
             return model;
        }

        @RequestMapping(value = "/spring/updateUser", method = RequestMethod.POST)
        public @ResponseBody String updateUser(@RequestParam("name") String name,@RequestParam("surname") String surname,
                                            @RequestParam("age") int age,@RequestParam("userId") Long userId){
                String result="false";
                User user =new User();

                try {
                        user.setName(name);
                        user.setSurname(surname);
                       /* user.setDob(df.parse(dob));*/

                        user.setAge(age);
                        user.setUser_id(userId);
                        boolean isUser= userService.updateUser(user,userId);
                        if(isUser){
                                result="true";
                        }
                } catch (Exception e) {
                        e.printStackTrace();
                }
                return result;
        }
        @RequestMapping(value = "/spring/deleteUser", method = RequestMethod.POST)
        public @ResponseBody String deleteUser(@RequestParam("userId") Long userId){
                String result="false";
                User user =new User();

                try {

                        user.setUser_id(userId);
                        boolean isUser= userService.deleteUser(userId);
                        if(isUser){
                                result="true";
                        }
                } catch (Exception e) {
                        e.printStackTrace();
                }
                return result;
        }


}
