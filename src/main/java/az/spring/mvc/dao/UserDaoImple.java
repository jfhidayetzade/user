package az.spring.mvc.dao;

import az.spring.mvc.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import javax.sql.DataSource;

import java.util.List;

public class UserDaoImple implements UserDao {

    @Autowired
    private  DataSource dataSource;

    public List<User> getUserList()throws Exception {
        JdbcTemplate jdbcTemplate=new JdbcTemplate();
        List<User> users =null;
        try{
            jdbcTemplate.setDataSource(dataSource);
            String sql="select * from springcoredb1.users   where active=1";
            users =jdbcTemplate.query(sql,new BeanPropertyRowMapper(User.class));
        }catch (Exception e){
            e.printStackTrace();
        }

        return users;
    }

    @Override
    public boolean addUser(User user) throws Exception {
        boolean result=false;
        try{
            JdbcTemplate jdbcTemplate=new JdbcTemplate(dataSource);
            String sql="insert into springcoredb1.users(name,surname,age)" +
                    " values (?,?,?)";
            if(user.getAge()>0){
            jdbcTemplate.update(sql,new Object[]{user.getName(), user.getSurname(), user.getAge()});
            result=true;}
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public User getUserById(long userId) {
        JdbcTemplate jdbcTemplate=new JdbcTemplate();
        User user =null;
        try{
           jdbcTemplate.setDataSource(dataSource);
           String sql="select user_id,name,surname,age from springcoredb1.users   where active=1 and user_id=?";
            user =(User) jdbcTemplate.queryForObject(sql,new Object[]{userId},new BeanPropertyRowMapper(User.class));
        }catch (Exception e){
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public boolean updateUser(User user, long userId) {
        boolean result=false;
        try{
            JdbcTemplate template=new JdbcTemplate(dataSource);
            String sql="update  springcoredb1.users set name=?,surname=?,age=? " +
                    " where user_id=?";
            if(user.getAge()>0){


            template.update(sql, new Object[]{user.getName(), user.getSurname(), user.getAge(),userId});
            result=true;}
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean deleteUser(long userId) {
        boolean result=false;
        try{
            JdbcTemplate template=new JdbcTemplate(dataSource);
            String sql="update  springcoredb1.users set active=0 \n" +
                    "where user_id=?";
            template.update(sql, new Object[]{userId});
            result=true;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return result;
    }
    /*public List<User> getGitList()throws Exception {
        JdbcTemplate jdbcTemplate=new JdbcTemplate();
        List<User> gits =null;
        try{
            jdbcTemplate.setDataSource(dataSource);
            String sql="select p. *,d.id_doctor, d.name doctor_name , d.surname doctor_surname from springcoredb1.patient p inner join springcoredb1.doctor d on p.doctor_id=d.id_doctor  where p.active=1";
            gits =jdbcTemplate.query(sql,new BeanPropertyRowMapper(User.class));
        }catch (Exception e){
            e.printStackTrace();
        }

        return gits;
    }*/
}
