package az.spring.mvc.model;
import java.time.*;
import java.util.*;

public class User {

    private long user_id;
    private String name;
    private String surname;
   /* private Date dob;*/
    private int age;
    private int active;

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
/*
    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }*/

    public int getAge() {
//        if(age>0){
//            return age;
//        }else{
            return age;
        //}

    }

    public void setAge(int age) {
       // if(age>0){
            this.age=age;
       /// }
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {

        this.active = active;
    }

    @Override
    public String toString() {
        return "User{" +
                "users_id=" + user_id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age='" + age + '\'' +
                ", active=" + active +
                '}';
    }
}
