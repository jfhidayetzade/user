package az.spring.mvc.service;

import az.spring.mvc.model.User;

import java.util.*;

public interface UserService {

    List<User> getUserList() throws Exception;
    boolean addUser(User user) throws Exception;
    User getUserById(long userId) ;
    boolean updateUser(User user, long userId);
    boolean deleteUser(long userId);
}
