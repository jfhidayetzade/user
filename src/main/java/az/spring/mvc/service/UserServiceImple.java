package az.spring.mvc.service;

import az.spring.mvc.dao.UserDao;
import az.spring.mvc.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImple implements UserService {

    @Autowired
    private UserDao userDao;

    public List<User> getUserList() throws Exception {
        return userDao.getUserList();
    }

    @Override
    public boolean addUser(User user) throws Exception {
        return userDao.addUser(user);
    }

    @Override
    public User getUserById(long userId) {
        return userDao.getUserById(userId);
    }

    @Override
    public boolean updateUser(User user, long userId) {
        return userDao.updateUser(user,userId);
    }

    @Override
    public boolean deleteUser(long userId) {
        return userDao.deleteUser(userId);
    }
}
