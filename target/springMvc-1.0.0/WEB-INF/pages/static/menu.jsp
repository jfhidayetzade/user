<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 7/9/2019
  Time: 18:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<ul class="sidebar-menu" data-widget="tree">
    <li class="header">Sechim</li>

    <li class="treeview active" >
        <a href="#"><i class="fa fa-link"></i> <span>User</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="${pageContext.request.contextPath}/spring/usersList">Users list</a></li>
            <li><a href="${pageContext.request.contextPath}/spring/newUser">New User</a></li>
        </ul>
    </li>
</ul>
