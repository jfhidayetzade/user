<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 4/29/2018
  Time: 01:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>$Title$</title>
</head>
<body>

<table border="1" width="100%">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Surname</th>
            <th>Dob</th>
            <th>Address</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach items="${patientList}" var="pl">
            <tr>
                <td>${pl.id_patient}</td>
                <td>${pl.name}</td>
                <td>${pl.surname}</td>
                <td>${pl.dob}</td>
                <td>${pl.address}</td>
            </tr>
        </c:forEach>
    </tbody>
</table>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery-3.4.1.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/main.js"></script>

</body>
</html>
